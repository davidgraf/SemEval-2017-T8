# SemEval 2017 - Task 8 - Implementation of Task A
Details about the challenge, the task descriptions and the dataset can be found at:
http://alt.qcri.org/semeval2017/task8/ 

## Introduction
This readme-file contains basic information about the SemEval-2017-T8 implementation of this repository.
More details about this implementation can be found in the related report documentation.

## Configuration
For reading the data, the path to the dataset has to be set to each "DATA_DIR" variable in the iodata-package.

## Structure
### iodata
Contains code for reading json-data

### features
Contains code for extracting and selecting features

### learning
Contains implementation and settings of different classifier

### main
Contains the main for running all the process-steps