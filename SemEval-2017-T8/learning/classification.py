'''
Created on 22.04.2017
@author: davidgraf
description: implementations of different classifier (using scikit) and settings
'''

from sklearn import svm
from sklearn import tree
from sklearn import gaussian_process
from sklearn import ensemble
from sklearn.model_selection import cross_val_score
from sklearn.neural_network import MLPClassifier


def SVMclassifierTrain(featureMatrix,labelMatrix):
    
    clf = svm.SVC()  
    print clf.fit(featureMatrix, labelMatrix)  
    
    # cross validation
    scores = cross_val_score(clf, featureMatrix, labelMatrix, cv=10)      
    print("Accuracy (Cross-V): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    
    return clf


def classifierPredict(featureMatrix,model):
    return model.predict(featureMatrix) 


def DecisionTreeTrain(featureMatrix,labelMatrix):
    
    clf = tree.DecisionTreeClassifier()  
    print clf.fit(featureMatrix, labelMatrix)  
    
    # cross validation
    scores = cross_val_score(clf, featureMatrix, labelMatrix, cv=10)      
    print("Accuracy (Cross-V): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    
    return clf


def GaussianProcessTrain(featureMatrix,labelMatrix):
    
    clf = gaussian_process.GaussianProcessClassifier()  
    print clf.fit(featureMatrix, labelMatrix)  
    
    # cross validation
    scores = cross_val_score(clf, featureMatrix, labelMatrix, cv=10)      
    print("Accuracy (Cross-V): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    
    return clf


def AdaBoostTrain(featureMatrix,labelMatrix):
    
    clf = ensemble.AdaBoostClassifier()  
    print clf.fit(featureMatrix, labelMatrix)  
    
    # cross validation
    scores = cross_val_score(clf, featureMatrix, labelMatrix, cv=10)      
    print("Accuracy (Cross-V): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    
    return clf


def RandomForestTrain(featureMatrix,labelMatrix):
    
    clf = ensemble.RandomForestClassifier(criterion="entropy")  
    print clf.fit(featureMatrix, labelMatrix)  
    
    # cross validation
    scores = cross_val_score(clf, featureMatrix, labelMatrix, cv=10)      
    print("Accuracy (Cross-V): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    
    return clf


def NeuroNetTrain(featureMatrix,labelMatrix):
    
    clf = MLPClassifier(solver='adam',max_iter=400,hidden_layer_sizes=(25,2))  
    print clf.fit(featureMatrix, labelMatrix)  
    
    # cross validation
    scores = cross_val_score(clf, featureMatrix, labelMatrix, cv=10)      
    print("Accuracy (Cross-V): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    
    return clf
