'''
Created on 06.05.2017
@author: david
description: save files for different purposes (analysis, further steps, ...)
parameter: DATA_DIR (path to file)
'''
import numpy as np

#variables
DATA_DIR = "D:/David/Studium/CS/2_Semester/IIS/Project/results/"


def saveTextToFile(text,filename):
  
    myfile = open(DATA_DIR+filename,"w") 
    try:
        myfile.write(text)
    except:
        myfile.write("Not possible to write text")
    myfile.close()
    
    
def saveMatrixToCSVFile(matrix,filename):     
    
    myfile = open(DATA_DIR + filename, "w")
    for l in matrix:
        try:
            for v in l:
                myfile.write(str(v) + ";")
            myfile.write("\n")
        except:
            myfile.write("error")
            
    myfile.close()
    