'''
Created on 22.04.2017
@author: davidgraf
description: runing the whole learning and prediction process
parameter: USE_TEST (if TRUE using test-set, otherwise only using splitted trainingset with 4:1 and Cross-Validation with Training-Set) 
           CLASSIFIER (Classifier using for prediction) 
               -> values: SVM, DecTree, GaussProc, AdaBoost, RandForest, NeuroNet
best result: 0,7664 using SVM
'''

from iodata.readJson import readJson
from iodata.savePredJson import savePredToFile
from features.preprocessing import featureExtraction
from iodata.readGTData import readGT
from learning.classification import SVMclassifierTrain, classifierPredict, DecisionTreeTrain, GaussianProcessTrain, AdaBoostTrain, RandomForestTrain, NeuroNetTrain
from sklearn.model_selection import train_test_split
import sklearn.metrics as scikitm

USE_TEST = True
CLASSIFIER = "SVM"

if __name__ == '__main__':
    
    print "started ..."
    
    # read Training data
    dataTrain, dataHierarchyTrain = readJson("train")
    gtTrain = readGT("trainA")  
    print "Training data read" 
    featureMatrixTrain, labelMatrixTrain, tweetMatrixTrain = featureExtraction(dataTrain, gtTrain, dataHierarchyTrain)
    print "Training features extracted"
    
    #read Test data
    if USE_TEST:
        dataTest, dataHierarchyTest = readJson("test")
        gtTest = readGT("testA")  
        print "Test data read"  
        featureMatrixTest, labelMatrixTest, tweetMatrixTest = featureExtraction(dataTest, gtTest, dataHierarchyTest)
        print "Test features extracted"
    
    print featureMatrixTrain
    #print labelMatrixTrain
    
    # split Train dataset into training and testing
    featureMatrixTrainSplitted, featureMatrixTestSplitted, labelMatrixTrainSplitted, labelMatrixTestSplitted = train_test_split(featureMatrixTrain, labelMatrixTrain, test_size=0.2, random_state=0)
    
    # train model
    if (CLASSIFIER=="SVM"):
        model = SVMclassifierTrain(featureMatrixTrain,labelMatrixTrain)
    if (CLASSIFIER=="DecTree"):
        model = DecisionTreeTrain(featureMatrixTrain,labelMatrixTrain)
    if (CLASSIFIER=="GaussProc"):
        model = GaussianProcessTrain(featureMatrixTrain,labelMatrixTrain)
    if (CLASSIFIER=="AdaBoost"):
        model = AdaBoostTrain(featureMatrixTrain,labelMatrixTrain)
    if (CLASSIFIER=="RandForest"):
        model = RandomForestTrain(featureMatrixTrain,labelMatrixTrain)
    if (CLASSIFIER=="NeuroNet"):
        model = NeuroNetTrain(featureMatrixTrain,labelMatrixTrain)
    
    # classify Train Testset
    predictedTrain = classifierPredict(featureMatrixTestSplitted,model)
    print "Accuracy (Testset Train): "+str(scikitm.accuracy_score(labelMatrixTestSplitted,predictedTrain))

    # classify test set
    if USE_TEST:
        predictedTest = classifierPredict(featureMatrixTest,model)
        print "Accuracy (Testset): "+str(scikitm.accuracy_score(labelMatrixTest,predictedTest))
        savePredToFile(tweetMatrixTest,predictedTest,"predictedATest.json")


    
    
    