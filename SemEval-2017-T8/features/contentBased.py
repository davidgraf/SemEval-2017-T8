'''
Created on 17.04.2017
@author: davidgraf
description: implementations of content-based features
'''

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import CountVectorizer
import re
import datetime as dt
import time as time
from networkx.algorithms import hierarchy
from asyncore import loop


def textSimToSource(tweetTexts):
      
    #calculate Jaccard Similarity (intersection/match Degree)
    matchDegree = float(len(tweetTexts[0].intersection(tweetTexts[1]))/float(len(tweetTexts[0].union(tweetTexts[0])))) 
    #print matchDegree
    return matchDegree  
    
def retweetCount(tweet): 
    return tweet["retweet_count"]

def supportWordsCount(tweetText):
    SUPPORTWORDS = set(["clarifi", "right", "evid", "confirm", "support", "definit", "discov", "explain", "truth", "true", "offici"])
    #tweetTokenList = tokenizer(tweetText) 
    #return len([token for token in SUPPORTWORDS if token in tweetTokenList])
    return len(tweetText.intersection(SUPPORTWORDS))

def commentWordsCount(tweetText):
    COMMENTWORDS = set(["comment", "claim", "accord", "sourc", "show", "captur", "say", "report", "observ", "footag"])
    return len(tweetText.intersection(COMMENTWORDS))

def denyWordsCount(tweetText):
    DENYWORDS = set(["incorrect", "accus", "contrari", "fals", "misstat", "wrong", "whi", "alleg", "fail", "deni", "untruth", "unsur", "not", "fuck"])
    return len(tweetText.intersection(DENYWORDS))

def questionWordsCount(tweetText):
    QUESTIONWORDS = set(["what", "who", "question", "whi", "how", "where", "wonder"])
    return len(tweetText.intersection(QUESTIONWORDS))

def questionMarks(tweetText):
    return len(re.findall("\?", tweetText))

def exclamationMarks(tweetText):
    return len(re.findall("!", tweetText))

def textLenght(tweetText):
    tweetTokenList = tokenizer(tweetText) 
    return len(tweetTokenList)

def linksCount(tweetText):
    return len(re.findall("http", tweetText))
    
def bowTokenizer(text):
    #nltk.download("stopwords")
    stopTerms = set(stopwords.words("english"))
    tokens = set(text.split()).difference(stopTerms)  
    return tokens

def tokenizer(text):   
    vectorizer = CountVectorizer(min_df=1)
    analyze = vectorizer.build_analyzer()
    return analyze(text)

def negationWordsCount(text):   
    NEGATIONWORDS = ["not","none","neither","never","no one","nobody","nor","nothing","nowhere","does not","did not","fuck"]
    count = 0

    for w in NEGATIONWORDS:
        count += len(re.findall(w, text))

    return count
    
def stemmer(terms):
    
    stemmer = PorterStemmer()
    stemmedTerms = set([]) 
     
    try: 
        for term in terms:
            stemmedTerm = stemmer.stem(term)
            stemmedTerms.add(str(stemmedTerm))
        
        return stemmedTerms
    except:
        return terms
    
def replyTimeToSource(sourceDate,replyDate):
    
    sourceTime = dt.datetime.strptime(sourceDate[:sourceDate.__len__()-11],'%a %b %d %H:%M:%S')
    replyTime = dt.datetime.strptime(replyDate[:replyDate.__len__()-11],'%a %b %d %H:%M:%S')
    timeDelta = replyTime - sourceTime
    return (timeDelta.days * 24 * 60) + (timeDelta.seconds/60)

    
def possiblySensitive(value):
    
    if value == "True": # no boolean -> text
        return 1
    else:
        return 0
    
def twitterClient(link):
    
    if link.find("Twitter Web Client") != -1: 
        return 1
    if link.find("Twitter for Mac") != -1: 
        return 1
    if link.find("Twitter for Websites") != -1: 
        return 1
    if link.find("Twitter for ") != -1: 
        return 0
    else:
        return 0
    
def tweetLevel(sourceId,targetId,hierarchy):
    
    count = 0
    tweethierachy = ""

    if sourceId == targetId:
        return 0
    else:
        for i, t in hierarchy.iteritems():
            if long(i) == sourceId:
                tweethierachy = str(t).replace("u", " ").split()
                break
    
        for x in tweethierachy:
            if (x.find("{") != -1):
                count = count + x.count('{')
            if (x.find("}") != -1):
                count = count - x.count('}')
            if (x.find(str(targetId)) != -1):
                return count
    
    return 0
            
    