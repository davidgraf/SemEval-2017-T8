'''
Created on 17.04.2017
@author: davidgraf
description: implementation of feature selection, preprocessing and scaling
'''
from features import userBased, contentBased
from features import featureEvaluation
import sklearn.preprocessing as pp
from sklearn.feature_selection import SelectKBest
import sklearn.covariance as cov
from iodata.saveToFile import saveMatrixToCSVFile
import numpy as np
from collections import Counter

def featureExtraction(data,gt,hierarchy): 
    
    featureMatrix = []
    labelMatrix = [] 
    tweetMatrix = []
    combinedMatrix = []
    features = ["RegTime",
                "Followers",
                "Friends",
                "Source-Reply-Sim",
                "retweets",
                "supportw",
                "commentw",
                "denyw",
                "queryw",
                "questMark",
                "textLengh",
                "LinkIncl",
                "NegWords",
                "replyTimeToSource",
                "TwitterClient",
                "NrUserInteractions",
                "Reply-To-Sim",
                "TweetHierarchy"]
    replyToTweetText = "-"
    clients = list()
    
    for sourceTweet, tweets in data.iteritems():
            for tweet, tweetContent in tweets.iteritems():
                #save SourceTweet
                for iTweet, iTweetContent in tweets.iteritems():         
                    if (sourceTweet==iTweet):
                        sourceTweetContent = iTweetContent
                        break
                    
                #save ReplyToTweet
                for jTweet, jTweetContent in tweets.iteritems():         
                    if (tweetContent["in_reply_to_status_id"]==jTweet):
                        replyToTweetContent = jTweetContent
                        replyToTweetText = jTweetContent["text"]
                        break    
                
                #tokenized and stemmed
                PPSourceTweetContent = contentBased.stemmer((contentBased.bowTokenizer(sourceTweetContent["text"])))
                PPTweetContent = contentBased.stemmer((contentBased.bowTokenizer(tweetContent["text"])))
                PPReplyToTweetContent = contentBased.stemmer((contentBased.bowTokenizer(replyToTweetText)))
                
                #creating class labels
                label = ""
                for t, l in gt.iteritems():
                    if long(t) == tweet:
                        label = str(l)
                
                clients.append(tweetContent["source"])                
                
                #tweet console output
                '''
                print PPSourceTweetContent
                print PPTweetContent
                print tweetContent["source"]
                
                if label == "deny":
                    try:
                        print tweetContent["text"]
                    except:
                        print "error"
                        
                if(label=="deny"):
                        try:
                            print str(tweet)+": "+ sourceTweetContent["text"] + "::==::" + tweetContent["text"]
                        except:
                            print "print not possible"
                '''
                        
                if label != "":
                #if label == "query":
                    labelMatrix.append(label)
                    tweetMatrix.append(tweet)               
                    #creating feature vector               
                    featureVector = ([userBased.userRegTime(tweetContent["user"]),
                                          userBased.userFollowers(tweetContent["user"]),
                                          userBased.userFriends(tweetContent["user"]),
                                          contentBased.textSimToSource([PPSourceTweetContent,PPTweetContent]),
                                          contentBased.retweetCount(tweetContent),
                                          contentBased.supportWordsCount(PPTweetContent),
                                          contentBased.commentWordsCount(PPTweetContent),
                                          contentBased.denyWordsCount(PPTweetContent),
                                          contentBased.questionWordsCount(PPTweetContent),
                                          contentBased.questionMarks(tweetContent["text"]),
                                          #contentBased.exclamationMarks(tweetContent["text"]),
                                          contentBased.textLenght(tweetContent["text"]),
                                          contentBased.linksCount(tweetContent["text"]),
                                          contentBased.negationWordsCount(tweetContent["text"]),
                                          contentBased.replyTimeToSource(sourceTweetContent["created_at"], tweetContent["created_at"]),
                                          #contentBased.possiblySensitive(tweetContent["possibly_sensitive"]),
                                          contentBased.twitterClient(tweetContent["source"]),
                                          userBased.userInteractions(tweetContent["user"], tweets),
                                          contentBased.textSimToSource([PPReplyToTweetContent,PPTweetContent]),
                                          contentBased.tweetLevel(sourceTweet,tweet,hierarchy)
                                        ])
                    
                    featureMatrix.append(featureVector[:len(features)]) #number of features
                    featureVector.append(label)
                    featureVector.append(str(tweet))
                    combinedMatrix.append(featureVector)               
 
    print "features:"                
    print features 
    
    nrClients = Counter(clients)
                                                
    # standardization (zero mean, variance of one)
    stdScale = pp.StandardScaler().fit(featureMatrix)
    featureMatrixScaled = stdScale.transform(featureMatrix)
    
    #file output
    saveMatrixToCSVFile(featureMatrix,"featureMatrix.csv")
    #saveMatrixToCSVFile(featureMatrixScaled,"featureScaleMatrix.csv")
    #saveMatrixToCSVFile(labelMatrix,"labelMatrix.csv")
    #saveMatrixToCSVFile(combinedMatrix,"featureLabelMatrix.csv")
     
    featureEvaluation.featureClassCoerr(featureMatrix,labelMatrix) 
                              
    #return pp.normalize(featureMatrixScaled), labelMatrix, tweetMatrix                    
    return featureSelection(featureMatrixScaled), labelMatrix, tweetMatrix        

def featureSelection(featureMatrix):
    return featureMatrix